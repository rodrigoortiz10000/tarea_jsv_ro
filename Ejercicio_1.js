// Declarar vector con 6 elementos de 3 distintos tipos de datos
let vector = [1, 2, 3, true, false, "Texto1", "Texto2"];


// a. Imprimir el vector en la consola
console.log(vector);

// b. Imprimir el primer y último elemento del vector usando índices
console.log('Primer elemento:', vector[0]);
console.log('Último elemento:', vector[vector.length - 1]);

// c. Modificar el valor del tercer elemento
vector[2] = false;

// d. Imprimir la longitud del vector
console.log('Longitud del vector:', vector.length);

// e. Agregar un elemento al final del vector usando "push"
vector.push('nuevo elemento');

// f. Eliminar el último elemento y mostrarlo en la consola usando "pop"
let ultimoElemento = vector.pop();
console.log('Último elemento eliminado:', ultimoElemento);

// g. Agregar un elemento en la mitad del vector usando "splice"
vector.splice(Math.floor(vector.length / 2), 0, 'elemento medio');

// h. Eliminar el primer elemento usando "shift"
let primerElemento = vector.shift();
console.log('Primer elemento eliminado:', primerElemento);

// i. Agregar de nuevo el mismo elemento al inicio del vector usando "unshift"
vector.unshift(primerElemento);

// Imprimir el vector actualizado en la consola
console.log('Vector actualizado:', vector);