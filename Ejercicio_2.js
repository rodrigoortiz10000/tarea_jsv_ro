// Declarar vector con 6 elementos de 3 distintos tipos de datos
let vector = [1, 2, 3, true, false, "Texto1", "Texto2"];

// a. Imprimir en la consola cada valor usando "for"
console.log("for:");
for (let i = 0; i < vector.length; i++) {
  console.log(vector[i]);
}

// b. Idem al anterior usando "forEach"
console.log("forEach:");
vector.forEach(function(elemento) {
  console.log(elemento);
});

// c. Idem al anterior usando "map"
console.log("map:");
vector.map(function(elemento) {
  console.log(elemento);
});

// d. Idem al anterior usando "while"
console.log("while:");
let index = 0;
while (index < vector.length) {
  console.log(vector[index]);
  index++;
};

// e. Idem al anterior usando "for..of"
console.log("for..of:");
for (let elemento of vector) {
  console.log(elemento);
};
